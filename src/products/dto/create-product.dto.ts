import { IsNotEmpty, Length } from 'class-validator';
import { OrderItem } from 'src/orders/entities/order-item';

export class CreateProductDto {
  @IsNotEmpty()
  @Length(4, 16)
  name: string;

  @IsNotEmpty()
  price: number;

  @IsNotEmpty()
  orderItem: OrderItem[];
}
